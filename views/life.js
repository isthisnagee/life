const html = require('choo/html')

const TITLE = ' 🗓  '
module.exports = view
function view (state, emit) {
  let h
  if (state.title !== TITLE) emit(state.events.DOMTITLECHANGE, TITLE)
  return html`
  <body>
    <div class="cf">
      <div class="w-100 w-50-ns fl">
       ${state.months.map(
         month => html`

         <div class="month dib mh2">
           <div class="month-name code"><small>${month}</small></div>

           ${state.year[month].map(
             (status, day) => html`
               <div onclick=${(e) => chooseOption(e, month, day)}
                    class="pointer mv1 db w1 h1 ba br1 b--gray bg-${status.color}">
               </div>
             `
           )}

         </div>
       `
       )}
       <button onclick=${() => fill()}>fill all</button>
       <button onclick=${() => clear()}>clear</button>
      </div>
      <div class="code w-100 w-50-ns fl">
        <p>playing around with the idea of cataloging emotions:</p>
        <p>direct inspiration from https://year-in-pixels.glitch.me</p>
        <p>But the main goal is a life-in-pixels, inspiration coming from here https://waitbutwhy.com/2014/05/life-weeks.html</p>
      </div>
    </div>
  </body>`

  function fill () {
    emit(state.events.day_fillAll)
  }

  function clear () {
    emit(state.events.day_clearAll)
  }

  function chooseOption (e, month, day) {
    const removeH = () => {
      if (h) {
        document.body.removeChild(h)
        h = undefined
      }
    }
    removeH()
    const x = e.clientX
    const y = e.clientY
    const style = `
      left: ${x}px;
      top: ${y}px;
      position: fixed;
      z-index: 1;
    `

    h = html`
      <div style=${style} class="ba flex flex-row items-center justify-center w4 h2 pa2 bg-white">
        <div onclick=${() => toggleDay(month, day, 'white')}
            class="dib ba mr1 w1 h1 pointer b--gray bw1 bg-white"></div>
        <div onclick=${() => toggleDay(month, day, 'red')}
            class="dib ba mr1 w1 h1 pointer b--gray bg-red"></div>
        <div onclick=${() => toggleDay(month, day, 'green')}
            class="dib ba mr1 w1 h1 pointer b--gray bg-green"></div>
        <div onclick=${() => toggleDay(month, day, 'blue')}
            class="dib ba mr1 w1 h1 pointer b--gray bg-blue"></div>
        <div onclick=${() => toggleDay(month, day, 'yellow')}
            class="dib ba mr1 w1 h1 pointer b--gray bg-yellow"></div>
        <div class="dib cf sans-serif b pointer"
             onclick=${() => removeH()}>
             x
        </div>
      </div>
    `
    document.body.appendChild(h)
  }

  function toggleDay (month, day, color) {
    emit(state.events.day_click, month, day, color)
  }
}
