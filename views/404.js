var html = require('choo/html')

var TITLE = ' 🗓  route not found'

module.exports = view

function view (state, emit) {
  if (state.title !== TITLE) emit(state.events.DOMTITLECHANGE, TITLE)
  return html`
    <body class="sans-serif">
      <h1 class="f-headline pa3 pa4-ns bg-light-pink">
        404 - 🗓  route not found
      </h1>
      <a href="/" class="ml3 ml4-ns link black underline">
        Back to main
      </a>
    </body>
  `
}
