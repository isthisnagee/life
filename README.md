# Life 

playing around with the idea of cataloging emotions: 

direct inspiration from https://year-in-pixels.glitch.me/

But the main goal is a life-in-pixels, inspiration coming from here https://waitbutwhy.com/2014/05/life-weeks.html

## Routes
Route              | File               | Description                     |
-------------------|--------------------|---------------------------------|
`/`              | `views/life.js`  | The yearview (for now)
`/*`             | `views/404.js`   | Display unhandled routes

## Commands
Command                | Description                                      |
-----------------------|--------------------------------------------------|
`$ npm start`        | Start the development server
`$ npm test`         | Lint, validate deps & run tests
`$ npm run build`    | Compile all files into `dist/`
`$ npm run create`   | Generate a scaffold file
`$ npm run inspect`  | Inspect the bundle's dependencies

