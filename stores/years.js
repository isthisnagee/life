module.exports = store

function rangeDays (month) {
  var year = new Date().getFullYear()
  return new Array(new Date(year, month, 0).getDate()).fill()
}

function oneOf (...options) {
  const numOptions = options.length
  const randOption = Math.floor(Math.random() * (numOptions))
  return options[randOption]
}

const months = [
  'jan',
  'feb',
  'mar',
  'apr',
  'may',
  'jun',
  'jul',
  'aug',
  'sep',
  'oct',
  'nov',
  'dec'
]

function store (state, emitter) {
  state.events.day_click = 'day:click'
  state.events.day_fillAll = 'day:fill-all'
  state.events.day_clearAll = 'day:clear-all'

  state.months = months
  state.year = months.reduce(
    (year, month, monthNum) =>
      Object.assign({}, year, {
        [month]: rangeDays(monthNum + 1).map(day => (
          { clicked: false,
            color: 'white'
          }
        ))
      }),
    {}
  )
  emitter.on('DOMContentLoaded', () => {
    emitter.on(state.events.day_click, (month, day, color = 'red') => {
      // month by name, day 0 indexed
      const d = state.year[month][day]
      d.color = color
      d.clicked = !d.clicked
      emitter.emit('render')
    })

    emitter.on(state.events.day_clearAll, () => {
      state.year = months.reduce(
        (year, month, monthNum) =>
        Object.assign({}, year, {
          [month]: rangeDays(monthNum + 1).map(day => (
            { clicked: false,
              color: 'white'
            }
          ))
        }),
        {}
      )
      emitter.emit('render')
    })

    emitter.on(state.events.day_fillAll, () => {
      state.year = months.reduce(
        (year, month, monthNum) =>
        Object.assign({}, year, {
          [month]: rangeDays(monthNum + 1).map(day => (
            { clicked: false,
              color: oneOf('red', 'green', 'yellow', 'blue')
            }
          ))
        }),
        {}
      )
      emitter.emit('render')
    })
  })
}
